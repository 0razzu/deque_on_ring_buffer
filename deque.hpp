#ifndef deque_hpp
#define deque_hpp


typedef double dequeData;


class Deque {
private:
    dequeData *a;
    unsigned left, buffer, size;
    short unsigned error_code;
    
public:
    Deque(unsigned);
    Deque(const Deque&);
    ~Deque();
    
    bool isEmpty();
    bool isFull();
    
    void clear();
    
    void pushLeft(dequeData);
    dequeData popLeft();
    dequeData peekLeft();
    void changeLeft(dequeData);
    void delLeft();
    
    void pushRight(dequeData);
    dequeData popRight();
    dequeData peekRight();
    void changeRight(dequeData);
    void delRight();
    
    short unsigned error();
};


#endif
