#include "deque.hpp"


enum errors {SIZE_ERR = 1, MEM_ERR, OVERFLOW, EMPTY};


Deque::Deque(unsigned size_t) {
    if (size_t < 2)
        error_code = SIZE_ERR;
    
    else {
        size = size_t;
        left = size - 1;
        buffer = size;
        a = new dequeData [size];
        
        if (!a)
            error_code = MEM_ERR;
        
        else
            error_code = 0;
    }
}


Deque::Deque(const Deque &deque0) {
    size = deque0.size;
    
    if (size < 2)
        error_code = SIZE_ERR;
    
    else if (!deque0.a)
        error_code = MEM_ERR;
    
    else {
        left = deque0.left;
        buffer = deque0.buffer;
        a = new dequeData [size];
        
        if (!a)
            error_code = MEM_ERR;
        
        else {
            for (unsigned i = 0; i < size; i++)
                a[i] = deque0.a[i];
            
            error_code = 0;
        }
    }
}


Deque::~Deque() {
    if (a)
        delete [] a;
}


bool Deque::isEmpty() {
    return buffer == size;
}


bool Deque::isFull() {
    return (left + 1) % size == buffer;
}


void Deque::clear() {
    left = size - 1;
    buffer = size;
}


void Deque::pushLeft(dequeData data) {
    if (!a)
        error_code = MEM_ERR;
    
    else if (isFull())
        error_code = OVERFLOW;
    
    else {
        left = (left + size - 1) % size;
        
        a[left] = data;
    }
    
    buffer %= size;
}


dequeData Deque::popLeft() {
    dequeData l = 0;
    
    if (!a)
        error_code = MEM_ERR;
    
    else if (isEmpty())
        error_code = EMPTY;
    
    else {
        l = a[left];
        
        left = (left + 1) % size;
        
        if ((left + 1) % size == buffer % size) {
            left = size - 1;
            buffer = size;
        }
    }
    
    return l;
}


dequeData Deque::peekLeft() {
    dequeData l = 0;
    
    if (!a)
        error_code = MEM_ERR;
    
    else if (isEmpty())
        error_code = EMPTY;
    
    else
        l = a[left];
    
    return l;
}


void Deque::changeLeft(dequeData data) {
    if (!a)
        error_code = MEM_ERR;
    
    else if (isEmpty())
        error_code = EMPTY;
    
    else
        a[left] = data;
}


void Deque::delLeft() {
    if (!a)
        error_code = MEM_ERR;
    
    else if (isEmpty())
        error_code = EMPTY;
    
    else {
        left = (left + 1) % size;
        
        if ((left + 1) % size == buffer % size) {
            left = size - 1;
            buffer = size;
        }
    }
}


void Deque::pushRight(dequeData data) {
    if (!a)
        error_code = MEM_ERR;
    
    else if (isFull())
        error_code = OVERFLOW;
    
    else {
        a[(buffer + size - 1) % size] = data;
        
        buffer = (buffer + 1) % size;
    }
}


dequeData Deque::popRight() {
    dequeData r = 0;
    
    if (!a)
        error_code = MEM_ERR;
    
    else if (isEmpty())
        error_code = EMPTY;
    
    else {
        buffer = (buffer + size - 1) % size;
        
        r = a[(buffer + size - 1) % size];
        
        if ((left + 1) % size == buffer % size) {
            left = size - 1;
            buffer = size;
        }
    }
    
    return r;
}


dequeData Deque::peekRight() {
    dequeData r = 0;
    
    if (!a)
        error_code = MEM_ERR;
    
    else if (isEmpty())
        error_code = EMPTY;
    
    else
        r = a[(buffer + size - 2) % size];
    
    return r;
}


void Deque::changeRight(dequeData data) {
    if (!a)
        error_code = MEM_ERR;
    
    else if (isEmpty())
        error_code = EMPTY;
    
    else
        a[(buffer + size - 2) % size] = data;
}


void Deque::delRight() {
    if (!a)
        error_code = MEM_ERR;
    
    else if (isEmpty())
        error_code = EMPTY;
    
    else {
        buffer = (buffer + size - 1) % size;
        
        if ((left + 1) % size == buffer % size) {
            left = size - 1;
            buffer = size;
        }
    }
}


short unsigned Deque::error() {
    short unsigned error_code_r = error_code;
    
    error_code = 0;
    
    return error_code_r;
}
